This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.  
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.  
You will also see any lint errors in the console.

## Application flow

1) Fetch stories IDs (set only 10 randoms stories IDs)  
2) Fetch each story by ID  
3) Fetch author data for every story by author id  
4) Show 10 stories in the browser and sort them by story score  

