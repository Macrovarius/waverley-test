import React from "react";
import styled from "styled-components";
import moment from 'moment';

const StoryItem = ({story}) => {
  const {
    url,
    time,
    score,
    title,
    authorId,
    authorKarma
  } = story;

  return (
    <StoryItemHolder>
      <StoryInnerItem>
        <StoryTitle>Story title:</StoryTitle>
        {title}
      </StoryInnerItem>
      {
        url &&
        <StoryInnerItem>
          <StoryTitle>Story URL:</StoryTitle>
          <a href={url} target="_blank" rel="noreferrer noopener">{url}</a>
        </StoryInnerItem>
      }
      <StoryInnerItem>
        <StoryTitle>Story time</StoryTitle>
        {moment(time).format('L')}
      </StoryInnerItem>
      <StoryInnerItem>
        <StoryTitle>Story score</StoryTitle>
        {score}
      </StoryInnerItem>
      <StoryInnerItem>
        <StoryTitle>Author id</StoryTitle>
        {authorId}
      </StoryInnerItem>
      <StoryInnerItem>
        <StoryTitle>Author karma score</StoryTitle>
        {authorKarma}
      </StoryInnerItem>
    </StoryItemHolder>
  )
}

export default StoryItem;

const StoryItemHolder = styled.div`
  height: 100%;
  border-radius: 8px;
  border: 1px solid #000;
  padding: 15px 15px 0;
  text-align: center;
  word-break: break-word;
`

const StoryInnerItem = styled.div`
  margin-bottom: 15px;
  line-height: 1.3;
`

const StoryTitle = styled.div`
  font-weight: 600;
`
