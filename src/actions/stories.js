import {
  SET_LOADING,
  SET_STORIES,
} from '../constants/actionTypes';

import { parseResponse, headers, API_URL } from '../helpers/fetchHelper'

export const fetchRandomsStories = (count) => dispatch => {
  dispatch(setLoading(true));
  return fetch(`${API_URL}/topstories.json`, {
    method: 'GET',
    headers: {
      ...headers,
    },
  })
    .then(parseResponse)
    .then(payload => {
      const randomArray = payload.sort(() => Math.random() - Math.random()).slice(0, count);

      fetchStoriesCollection(randomArray, dispatch)
    })
    .catch(console.log);
};

export const fetchCurrentStory = (id) => {
  return fetch(`${API_URL}/item/${id}.json`, {
    method: 'GET',
    headers: {
      ...headers,
    },
  })
    .then(parseResponse)
    .then(fetchCurrentAuthor)
    .catch(console.log);
};

export const fetchCurrentAuthor = (story) => {
  return fetch(`${API_URL}/user/${story.by}.json`, {
    method: 'GET',
    headers: {
      ...headers,
    },
  })
    .then(parseResponse)
    .then((res) => {
      return {
        ...story,
        authorId: res.id,
        authorKarma: res.karma
      }
    })
    .catch(console.log);
};

export const fetchStoriesCollection = (randomArray, dispatch) => {
  Promise.all(randomArray.map(storyId => fetchCurrentStory(storyId)))
    .then(payload => {
      dispatch({ type: SET_STORIES, payload });
      dispatch(setLoading(false));
    }).catch(() => setLoading(false))
};

export const setLoading = payload => ({ type: SET_LOADING, payload });
