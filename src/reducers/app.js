import {
  SET_LOADING
} from '../constants/actionTypes';

const initialState = {
  loader: false,
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_LOADING:
      return {
        ...state,
        loader: payload
      };

    default:
      return state;
  }
};
