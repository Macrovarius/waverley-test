import {
  SET_STORIES
} from '../constants/actionTypes';

const initialState = {
  storiesCollection: []
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_STORIES:
      return {
        ...state,
        storiesCollection: payload
      };

    default:
      return state;
  }
};
