export const headers = {
  Pragma: 'no-cache',
  Accept: 'application/json',
  Expires: -1,
  'Content-Type': 'application/json',
  'Cache-Control': 'no-cache',
};

export const parseResponse = res => res.json();

export const API_URL = 'https://hacker-news.firebaseio.com/v0/';
