import React, {useEffect} from "react";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import styled from 'styled-components';

import { fetchRandomsStories } from '../../actions/stories';

import { StoryItem, Loader } from '../../components';

const Stories = (props) => {
  const {
    loader,
    storiesCollection,
    fetchRandomsStories
  } = props;

  const fetchData = () => {
    fetchRandomsStories(10)
  };

  useEffect(fetchData, []);


  if (loader) {
    return <Loader />
  }

  return (
    <StoriesListHolder>
      {
        storiesCollection.length > 0 ?
        storiesCollection.sort((a, b) => a.score - b.score).map(story => (
          <StoryHolder key={story.id}>
            <StoryItem {...{story}} />
          </StoryHolder>
        )) : <EmptyList>Empty list</EmptyList>
      }
    </StoriesListHolder>
  )
}

const mapStateToProps = state => {
  const {
    app,
    stories
  } = state;

  return {
    loader: app.loader,
    storiesCollection: stories.storiesCollection
  }
};

const mapDispatchToProps = dispatch => bindActionCreators({
  fetchRandomsStories
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Stories);


const StoriesListHolder = styled.div`
  display: flex;
  align-items: stretch;
  flex-wrap: wrap;
  width: 100%;
  max-width: 800px;
  margin: 0 auto;
  padding: 50px 0;
  
  @media (max-width: 767px) {
    justify-content: center;
  }
`
const StoryHolder = styled.div`
  width: calc(100% / 2);
  margin-bottom: 20px;
  padding: 0 10px;
  
  @media (max-width: 767px) {
    width: 100%;
    max-width: 320px;
  }
`
const EmptyList = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;
  min-height: 300px;
`
